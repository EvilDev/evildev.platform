require "rubygems"
require 'bundler'
require 'fileutils'
require 'pathname'

system "bundle install --system"
Gem.clear_paths

require 'albacore'
require 'noodle'
require "rexml/document"

include REXML

Dir.glob('**/*.rake').each { |r| import r }

solution_file = FileList["**/*.sln"].first
ZIP_EXE = ENV['ZIP_EXE_PATH'] || '"C:\Program Files\7-Zip\7z.exe"'
NUGET = ENV['NUGET_EXE_PATH'] || FileList[".nuget/nuget.exe"].first
XUNIT = FileList["packages/xunit.runners.**/tools/xunit.console.clr4.exe"].first
OPENCOVER = ENV['OPENCOVER_PATH']

desc 'Builds the application'
task :default => ["build:packages"]

desc 'Builds the application'
task :build, [:config] => ["build:packages"]

namespace :build do
	msbuild :solution, [:config] do |msb, args|
		msb.properties = {
			:configuration => args[:config] || :Debug,
			:platform => "Any CPU"
		}

		msb.targets :Build
		msb.solution = solution_file
	end
end

namespace :test do
	exec :unit, [:config] do |cmd, args|
		assemblies = FileList["Tests/**.Tests/bin/#{args[:config] || :Debug}/*.Tests.dll"].uniq

		cmd.command = OPENCOVER
		cmd.parameters = ["-target:\"#{XUNIT}\"", "-targetargs:\"

	end

	xunit :assembly, [:path] do |xunit, args|
        puts "Content Directory: #{assemblies}"
		xunit.command = XUNIT
		xunit.assembly = 
		#xunit.options = ["/teamcity"]
	end



	desc "Runs all unit tests"
	xunit :unit, [:config] do |xunit, args|
		puts "**** Starting Unit Tests ****"
		
		assemblies = FileList["Tests/**.Tests/bin/#{args[:config] || :Debug}/*.Tests.dll"].uniq
        puts "Content Directory: #{assemblies}"
		xunit.command = XUNIT
		xunit.assemblies = assemblies
		#xunit.options ENV['XUNIT_OPTS'] || "/teamcity"
	end
end