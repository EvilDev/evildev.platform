﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using Moq;
using Moq.Language.Flow;

using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using Ploeh.AutoFixture.Dsl;

namespace EvilDev.Commons.Testing
{
    public abstract class Specification
    {
        protected IFixture Fixture { get; private set; }

        protected Specification(IFixture fixture)
        {
            Fixture = fixture;
        }

        protected Specification()
            : this(new Fixture().Customize(new AutoMoqCustomization()))
        {
        }

        protected TDep Chain<TDep, TChild>(Expression<Func<TDep, TChild>> expression, bool verify = false)
            where TDep : class
            where TChild : class
        {
            Func<Mock<TDep>, IReturnsResult<TDep>> func = x => x.Setup(expression).Returns(Dep<TChild>());

            return verify ? Dep<TDep>(x => func(x).Verifiable()) : Dep<TDep>(x => func(x));
        }

        protected T Dep<T>(Action<Mock<T>> action) where T : class
        {
            var mock = Fixture.Freeze<Mock<T>>();
            action(mock);
            return mock.Object;
        }

        protected T Dep<T>() where T : class
        {
            Fixture.Freeze<Mock<T>>();
            return Fixture.Freeze<T>();
        }

        protected T Dep<T>(T instance) where T : class
        {
            Fixture.Register(() => instance);
            return Fixture.Freeze<T>();
        }

        protected T Fake<T>()
        {
            return Fixture.Create<T>();
        }

        protected T Fake<T>(Action<Mock<T>> action) where T : class
        {
            var mock = Fixture.Create<Mock<T>>();
            action(mock);
            return mock.Object;
        }

        protected T Fake<T>(Action<ICustomizationComposer<T>> builder)
        {
            var item = Fixture.Build<T>();

            builder(item);

            return item.Create();
        }

        protected IEnumerable<T> FakeMany<T>()
        {
            return Fixture.CreateMany<T>();
        }

        protected IEnumerable<T> FakeMany<T>(Func<ICustomizationComposer<T>, IPostprocessComposer<T>> builder)
        {
            var item = Fixture.Build<T>();
            return builder(item).CreateMany<T>();
        }

        protected void Verify<T>() where T : class
        {
            Dep<T>(x => x.VerifyAll());
        }
    }
}