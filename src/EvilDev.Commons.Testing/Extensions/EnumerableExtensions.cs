﻿using System;
using System.Collections.Generic;

namespace EvilDev.Commons.Testing.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            return enumerable.ForEach((i, item) => action(item));
        } 

        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> enumerable, Action<int, T> action)
        {
            var i = 0;

            foreach (var item in enumerable)
            {
                action(i, item);
                i++;
                yield return item;
            }
        } 
    }
}