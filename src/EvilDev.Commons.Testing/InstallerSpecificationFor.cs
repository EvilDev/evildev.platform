﻿using System;
using System.Collections.Generic;
using System.Linq;

using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using EvilDev.Commons.Testing.Extensions;

using FluentAssertions;

namespace EvilDev.Commons.Testing
{
    public abstract class InstallerSpecificationFor<TInstaller> :
        AutoMockSpecificationFor<TInstaller, IWindsorInstaller>
        where TInstaller : class, IWindsorInstaller
    {
        protected InstallerSpecificationFor()
        {
            Container = new WindsorContainer();
            Dep(Container);

            RegisteredComponents = new Dictionary<string, IHandler>();
            ExpectedComponents = new Dictionary<Type, string>();

            Container.Kernel.ComponentRegistered += (key, handler) => RegisteredComponents.Add(key, handler);
        }

        protected IWindsorContainer Container { get; private set; }

        protected IDictionary<Type, string> ExpectedComponents { get; private set; }

        protected IDictionary<string, IHandler> RegisteredComponents { get; private set; }

        protected void Expect<TService>()
        {
            ExpectedComponents.Add(typeof(TService), null);
        }

        protected void Expect<TService>(string key)
        {
            ExpectedComponents.Add(typeof(TService), key);
        }

        protected void Install()
        {
            Sut.Install(Dep<IWindsorContainer>(), Dep<IConfigurationStore>());
        }

        protected void ShouldHaveAddedFacility(Type facility)
        {
            Container.Kernel.GetFacilities().Any(f => f.GetType() == facility).Should().BeTrue();
        }

        protected void ShouldHaveAddedFacility<TFacility>() where TFacility : IFacility
        {
            ShouldHaveAddedFacility(typeof(TFacility));
        }

        protected void ShouldHaveHandlerFor<T>()
        {
            ShouldHaveHandlerFor(typeof(T));
        }

        protected void ShouldHaveHandlerFor(Type type)
        {
            var component = RegisteredComponents.FirstOrDefault(x => x.Value.Supports(type));

            component.Should().NotBeNull();
        }

        protected void ShouldHaveHandlerFor(Type type, string key)
        {
            var component = RegisteredComponents.FirstOrDefault(x => x.Value.Supports(type) && x.Key == key);

            component.Should().NotBeNull();
        }

        protected void Verify()
        {
            ExpectedComponents.ForEach(
                pair =>
                    {
                        if (pair.Value == null)
                        {
                            ShouldHaveHandlerFor(pair.Key);
                        }
                        else
                        {
                            ShouldHaveHandlerFor(pair.Key, pair.Value);
                        }
                    });

            RegisteredComponents.Select(r => r.Value.ComponentModel.Services.First())
                                .Should()
                                .BeEquivalentTo(ExpectedComponents.Select(ec => ec.Key));
        }
    }
}