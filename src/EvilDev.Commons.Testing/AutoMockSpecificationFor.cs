﻿using Ploeh.AutoFixture;
using Ploeh.AutoFixture.Kernel;

namespace EvilDev.Commons.Testing
{
    public abstract class AutoMockSpecificationFor<TSut, TContract> : Specification
        where TSut : class, TContract where TContract : class
    {
        protected AutoMockSpecificationFor()
        {
            // Override default behavior for the SUT object, by using the greediest constructor, instead of the laziest constructor.
            Fixture.Customize<TSut>(c => c.FromFactory(new MethodInvoker(new GreedyConstructorQuery())));
        }

        protected AutoMockSpecificationFor(IFixture fixture)
            : base(fixture)
        {
            Fixture.Customize<TSut>(c => c.FromFactory(new MethodInvoker(new GreedyConstructorQuery())));
        } 

        protected virtual TContract Sut
        {
            get
            {
                return Fixture.Freeze<TSut>();
            }
        }
    }
}