﻿using Ploeh.AutoFixture;

namespace EvilDev.Commons.Testing
{
    public abstract class ExtensionMethodSpecificationFor<TExtended> : Specification
    {
        protected ExtensionMethodSpecificationFor()
        {
            Fixture = new Fixture();
        }

        protected IFixture Fixture { get; private set; }
    }
}