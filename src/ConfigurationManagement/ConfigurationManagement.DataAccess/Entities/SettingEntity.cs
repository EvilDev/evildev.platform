﻿namespace ConfigurationManagement.DataAccess.Entities
{
    public class SettingEntity
    {
        public virtual string Category { get; set; }

        public virtual string Value { get; set; }

        public virtual string Key { get; set; }
    }
}