﻿namespace ConfigurationManagement.DataAccess.Entities
{
    public class EndPointEntity
    {
        public virtual string Name { get; set; }

        public virtual string Value { get; set; }
    }
}