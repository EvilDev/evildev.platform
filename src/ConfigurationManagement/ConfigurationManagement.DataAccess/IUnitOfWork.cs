﻿using System.Linq;

namespace ConfigurationManagement.DataAccess
{
    public interface IUnitOfWork
    {
        IQueryable<T> Query<T>();

        void Add<T>(T entity);

        void Remove<T>(T entity);

        void Update<T>(T entity);
    }
}