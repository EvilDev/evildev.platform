﻿using System.Collections.Generic;
using System.Linq;

using ConfigurationManagement.DataAccess.Entities;
using ConfigurationManagement.DataAccess.Factories;

namespace ConfigurationManagement.DataAccess.Repositories
{
    public class SettingRepository : Repository<SettingEntity>, ISettingRepository
    {
        public SettingRepository(IUnitOfWorkFactory factory)
            : base(factory)
        {
        }

        public IDictionary<string, string> this[string category]
        {
            get
            {
                return Query.Where(x => x.Category == category).ToDictionary(x => x.Key, x => x.Value);
            }
        }

        public void Add(string category, string key, string value)
        {
            Add(new SettingEntity { Category = category, Key = key, Value = value });
        }

        public void Remove(string category, string key)
        {
            var setting = Query.FirstOrDefault(x => x.Category == category && x.Key == key);

            Remove(setting);
        }

        public void Update(string category, string key, string value)
        {
            var setting = Query.FirstOrDefault(x => x.Category == category && x.Key == key);
            setting.Value = value;
            Update(setting);
        }
    }
}