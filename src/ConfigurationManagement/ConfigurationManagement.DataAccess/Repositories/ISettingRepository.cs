﻿using System.Collections.Generic;

namespace ConfigurationManagement.DataAccess.Repositories
{
    public interface ISettingRepository
    {
        IDictionary<string, string> this[string category] { get; }

        void Add(string category, string key, string value);

        void Remove(string category, string key);

        void Update(string category, string key, string value);
    }
}