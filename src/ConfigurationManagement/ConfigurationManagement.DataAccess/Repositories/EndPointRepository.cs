﻿using System.Linq;

using ConfigurationManagement.DataAccess.Entities;
using ConfigurationManagement.DataAccess.Factories;
using ConfigurationManagement.Infrastructure;

namespace ConfigurationManagement.DataAccess.Repositories
{
    public class EndPointRepository : Repository<EndPointEntity>, IEndPointRepository
    {
        private readonly IEncryptionProvider _crypto;

        public EndPointRepository(IUnitOfWorkFactory factory, IEncryptionProvider crypto)
            : base(factory)
        {
            _crypto = crypto;
        }

        public string this[string name]
        {
            get
            {
                return _crypto.Decrypt(Query.First(x => x.Name == name).Value);
            }
        }

        public void Add(string name, string endPoint)
        {
            var endpoint = new EndPointEntity { Name = name, Value = _crypto.Encrypt(endPoint) };
            Add(endpoint);
        }

        public void Remove(string name)
        {
            var endpoint = Query.FirstOrDefault(x => x.Name == name);

            Remove(endpoint);
        }

        public void Update(string name, string endPoint)
        {
            var entity = Query.FirstOrDefault(x => x.Name == name);
            entity.Value = _crypto.Encrypt(endPoint);
            Update(entity);
        }
    }
}