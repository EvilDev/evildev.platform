﻿namespace ConfigurationManagement.DataAccess.Repositories
{
    public interface IEndPointRepository
    {
        string this[string name] { get; }

        void Add(string name, string endPoint);

        void Remove(string name);

        void Update(string name, string endPoint);
    }
}