﻿namespace ConfigurationManagement.DataAccess.Factories
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create();
    }
}