﻿using System.Linq;

using ConfigurationManagement.DataAccess.Factories;

namespace ConfigurationManagement.DataAccess
{
    public abstract class Repository<TEntity>
    {
        private readonly IUnitOfWorkFactory _factory;

        protected Repository(IUnitOfWorkFactory factory)
        {
            _factory = factory;
        }

        protected IQueryable<TEntity> Query
        {
            get
            {
                return _factory.Create().Query<TEntity>();
            }
        }

        protected void Add(TEntity entity)
        {
            _factory.Create().Add(entity);
        }

        protected void Remove(TEntity entity)
        {
            _factory.Create().Remove(entity);
        }

        protected void Update(TEntity entity)
        {
            _factory.Create().Update(entity);
        }
    }
}