﻿using System;
using System.Linq;

using NHibernate;
using NHibernate.Linq;

namespace ConfigurationManagement.DataAccess.Nhibernate
{
    public class NhibernateUnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly ISession _session;

        private readonly ITransaction _transaction;

        public NhibernateUnitOfWork(ISessionFactory sessionFactory)
        {
            _session = sessionFactory.OpenSession();
            _transaction = _session.BeginTransaction();
        }

        public void Add<T>(T entity)
        {
            _session.Save(entity);
        }

        public void Dispose()
        {
            try
            {
                _transaction.Commit();
            }
            finally
            {
                _transaction.Rollback();
                _transaction.Dispose();
                _session.Dispose();
            }
        }

        public IQueryable<T> Query<T>()
        {
            return _session.Query<T>();
        }

        public void Remove<T>(T entity)
        {
            _session.Delete(entity);
        }

        public void Update<T>(T entity)
        {
            _session.Update(entity);
        }
    }
}