﻿using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Registration;

using EvilDev.Commons.Settings;

using SettingsProvider = ConfigurationManagement.Plumbing.SettingsProvider;

namespace ConfigurationManagement.Facilities
{
    public class SettingsFacility : AbstractFacility
    {
        private ISettingsProvider _settingsProvider;

        public SettingsFacility()
            : this(new AppSettingsProvider())
        {
        }

        public SettingsFacility(ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
        }

        protected override void Init()
        {
            Kernel.Register(Component.For<ISettingsProvider>().Instance(_settingsProvider));

            SettingsProvider.Current = _settingsProvider;
        }

        public SettingsFacility Use(ISettingsProvider provider)
        {
            _settingsProvider = provider;
            return this;
        }
    }
}