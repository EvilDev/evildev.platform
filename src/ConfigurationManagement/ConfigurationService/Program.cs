﻿using System;

using Nancy.Hosting.Self;

using Topshelf;

namespace ConfigurationManagement
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var uri = new Uri("http://localhost:3579");

            var host = HostFactory.New(c =>
                {
                    c.UseNLog();

                    c.Service<NancyHost>(
                        s =>
                        {
                            s.ConstructUsing(() => new NancyHost(uri));
                            s.WhenStarted(h => h.Start());
                            s.WhenStopped(h => h.Stop());
                            s.WhenShutdown(h => h.Dispose());
                        });

                    c.RunAsLocalSystem();
                    c.SetServiceName("Evildev.Platform");
                });

            host.Run();
        }
    }
}