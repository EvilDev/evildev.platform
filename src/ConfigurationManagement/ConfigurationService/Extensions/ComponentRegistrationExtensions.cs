﻿using Castle.MicroKernel.Registration;

using ConfigurationManagement.Plumbing;

namespace ConfigurationManagement.Extensions
{
    public static class ComponentRegistrationExtensions
    {
        public static ComponentRegistration<T> DependsOnSetting<T>(this ComponentRegistration<T> registration, string dependencyName, string setting, string defaultValue) where T : class
        {
            var settings = SettingsProvider.Current;
            if (!settings.ContainsKey(setting))
            {
                settings[setting] = defaultValue;
            }

            return registration.DependsOn(Dependency.OnValue(dependencyName, settings[setting]));
        }
    }
}