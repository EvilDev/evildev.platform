﻿using System;

using Castle.Windsor;
using Castle.Windsor.Installer;

using NLog;

using Nancy;
using Nancy.Bootstrapper;
using Nancy.Bootstrappers.Windsor;
using Nancy.Conventions;
using Nancy.Diagnostics;
using Nancy.Responses;

namespace ConfigurationManagement
{
    public class Bootstrapper : WindsorNancyBootstrapper
    {
        protected override void ConfigureConventions(NancyConventions conventions)
        {
            base.ConfigureConventions(conventions);

            conventions.StaticContentsConventions.AddDirectory("/scripts", "/scripts");
        }

        protected override DiagnosticsConfiguration DiagnosticsConfiguration
        {
            get { return new DiagnosticsConfiguration { Password = @"p@ssw0rd" }; }
        }

        protected override void ApplicationStartup(IWindsorContainer container, IPipelines pipelines)
        {
            pipelines.OnError += HandleError;

            base.ApplicationStartup(container, pipelines);
        }

        protected override void RequestStartup(IWindsorContainer container, IPipelines pipelines, NancyContext context)
        {
            pipelines.OnError.AddItemToEndOfPipeline(
                (z, a) =>
                    {
                        LogManager.GetCurrentClassLogger()
                                  .FatalException("Unhandled error on request: " + context.Request.Url + " : " + a.Message, a);

                        return z.Response;
                    });

            base.RequestStartup(container, pipelines, context);
        }

        private Response HandleError(NancyContext context, Exception ex)
        {
            LogManager.GetCurrentClassLogger().FatalException("Unhandled error on request: " + context.Request.Url + " : " + ex.Message, ex);

            if (context.Request.Headers.ContentType == "text/html")
            {
                return new RedirectResponse("~/error");
            }

            return context.Response;
        }

        protected override void ConfigureApplicationContainer(IWindsorContainer container)
        {
            base.ConfigureApplicationContainer(container);

            container.Install(FromAssembly.InThisApplication());
        }
    }
}