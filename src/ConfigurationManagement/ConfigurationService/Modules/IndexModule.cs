﻿using ConfigurationManagement.DataAccess.Repositories;

using Nancy;

namespace ConfigurationManagement.Modules
{
    public class IndexModule : NancyModule
    {
        public IndexModule(ISettingRepository settings)
        {
            Get["/"] = parameters => View["index"];

            Get["/settings/{category}"] = x => View["settings", new { Category = x.Category, Settings = settings[x.Category] }];
        }
    }
}