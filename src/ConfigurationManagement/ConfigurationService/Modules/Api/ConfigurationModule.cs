﻿using ConfigurationManagement.DataAccess.Repositories;

using Nancy;

namespace ConfigurationManagement.Modules.Api
{
    public class ConfigurationModule : NancyModule
    {
        public ConfigurationModule(ISettingRepository settings, IEndPointRepository endPoints)
            : base("/api/v1/configuration")
        {
            this.Get["/settings/{category}"] = x => settings[x.Category];

            this.Get["/settings/{category}/{key}"] = x => settings[x.Category][x.Key];

            this.Post["/settings/{category}/{key}"] = x => settings.Add(x.Category, x.Key, x.Value);

            this.Delete["/settings/{category}/{key}"] = x => settings.Remove(x.Category, x.Key);

            this.Put["/settings/{category}/{key}"] = x => settings.Update(x.Category, x.Key, x.Value);

            this.Get["/endpoint/{name}"] = x => endPoints[x.Name];

            this.Post["/endpoint/{name}"] = x => endPoints.Add(x.Name, x.EndPoint);

            this.Delete["/endpoint/{name}"] = x => endPoints.Remove(x.Name);

            this.Put["/endpoint/{name}"] = x => endPoints.Update(x.Name, x.EndPoint);
        }
    }
}