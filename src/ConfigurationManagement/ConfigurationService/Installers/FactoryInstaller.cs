﻿using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace ConfigurationManagement.Installers
{
    public class FactoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Types.FromAssemblyInThisApplication()
                                    .Where(t => t.Name.EndsWith("Factory"))
                                    .Configure(cr => cr.AsFactory()));
        }
    }
}