﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using ConfigurationManagement.DataAccess;

namespace ConfigurationManagement.Installers
{
    public class RepositoryInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyInThisApplication()
                                      .BasedOn(typeof(Repository<>))
                                      .WithServiceDefaultInterfaces());
        }
    }
}