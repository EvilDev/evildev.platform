﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using ConfigurationManagement.DataAccess;
using ConfigurationManagement.DataAccess.Db4o;
using ConfigurationManagement.Extensions;
using ConfigurationManagement.Facilities;
using ConfigurationManagement.Infrastructure;
using ConfigurationManagement.Plumbing;

using Nancy.Bootstrappers.Windsor;

namespace ConfigurationManagement.Installers
{
    public class PlumbingInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.AddFacility<SettingsFacility>(x => x.Use(new ProtectedAppSettingsProvider()));

            container.Register(Component.For<IUnitOfWork>()
                                        .ImplementedBy<Db4OUnitOfWork>()
                                        .DependsOnSetting("connectionString", AppSettings.Database, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Data\config.db"))
                                        .LifestyleScoped<NancyPerWebRequestScopeAccessor>());

            var algorithm = SymmetricAlgorithm.Create();
            algorithm.GenerateKey();
            algorithm.GenerateIV();

            container.Register(
                Component.For<IEncryptionProvider>()
                         .ImplementedBy<SymmetricEncryptionProvider>()
                         .DependsOnSetting("key", AppSettings.EncryptionKey, Encoding.Default.GetString(algorithm.Key))
                         .DependsOnSetting("iv", AppSettings.EncryptionIVs, Encoding.Default.GetString(algorithm.IV)));
        }
    }
}