﻿namespace ConfigurationManagement.Installers
{
    public static class AppSettings
    {
        public const string Database = "Database";

        public const string EncryptionKey = "EncryptionKey";

        public const string EncryptionIVs = "EncryptionIVs";
    }
}