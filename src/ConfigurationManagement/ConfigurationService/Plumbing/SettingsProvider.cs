﻿using EvilDev.Commons.Settings;

namespace ConfigurationManagement.Plumbing
{
    public static class SettingsProvider
    {
        public static ISettingsProvider Current { get; set; }
    }
}