﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

using ConfigurationManagement.Infrastructure;

namespace ConfigurationManagement.Plumbing
{
    public class SymmetricEncryptionProvider : IEncryptionProvider
    {
        private readonly SymmetricAlgorithm _algorithm;

        private readonly byte[] _iv;

        private readonly byte[] _key;

        public SymmetricEncryptionProvider(string key, string iv)
        {
            _key = Encoding.Default.GetBytes(key);
            _iv = Encoding.Default.GetBytes(iv);
            _algorithm = SymmetricAlgorithm.Create();
        }

        public string Decrypt(string source)
        {
            return Transform(source, _algorithm.CreateDecryptor(_key, _iv));
        }

        public string Encrypt(string source)
        {
            return Transform(source, _algorithm.CreateEncryptor(_key, _iv));
        }

        private static string Transform(string text, ICryptoTransform cryptoTransform)
        {
            using (var stream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(stream, cryptoTransform, CryptoStreamMode.Write))
                {
                    var input = Encoding.Default.GetBytes(text);

                    cryptoStream.Write(input, 0, input.Length);
                    cryptoStream.FlushFinalBlock();

                    return Encoding.Default.GetString(stream.ToArray());
                }
            }
        }
    }
}