﻿using System.Configuration;
using System.Linq;

using EvilDev.Commons.Settings;

namespace ConfigurationManagement.Plumbing
{
    public class ProtectedAppSettingsProvider : ISettingsProvider
    {
        private readonly Configuration _config;

        public ProtectedAppSettingsProvider()
        {
            _config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            Protect();
        }

        public string this[string key]
        {
            get
            {
                return _config.AppSettings.Settings[key].Value;
            }

            set
            {
                _config.AppSettings.Settings.Add(key, value);
                _config.Save();
            }
        }

        public bool ContainsKey(string key)
        {
            return _config.AppSettings.Settings.AllKeys.AsEnumerable().Contains(key);
        }

        private void Protect()
        {
            var section = _config.GetSection("appSettings");
            if (!section.SectionInformation.IsProtected)
            {
                section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
            }

            _config.Save();
        }
    }
}