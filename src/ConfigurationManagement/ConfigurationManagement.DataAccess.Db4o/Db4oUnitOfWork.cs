﻿using System;
using System.IO;
using System.Linq;

using Db4objects.Db4o;

namespace ConfigurationManagement.DataAccess.Db4o
{
    public class Db4OUnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly IObjectContainer _container;

        public Db4OUnitOfWork(IObjectContainer container)
        {
            _container = container;
        }

        public Db4OUnitOfWork(string connectionString)
            : this(GetContainer(connectionString))
        {
        }

        private static IObjectContainer GetContainer(string path)
        {
            var dataDir = Path.GetDirectoryName(path);

            if (!Directory.Exists(dataDir))
            {
                Directory.CreateDirectory(dataDir);
            }

            return Db4oEmbedded.OpenFile(path);
        }

        public IQueryable<T> Query<T>()
        {
            return _container.Query<T>().AsQueryable();
        }

        public void Add<T>(T entity)
        {
            _container.Store(entity);
        }

        public void Remove<T>(T entity)
        {
            _container.Delete(entity);
        }

        public void Update<T>(T entity)
        {
            _container.Store(entity);
        }

        public void Dispose()
        {
            try
            {
                _container.Commit();
            }
            catch (Exception)
            {
                _container.Rollback();
                throw;
            }
            finally
            {
                _container.Dispose();
            }
        }
    }
}