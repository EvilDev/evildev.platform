﻿using System;
using System.Linq;

using Db4objects.Db4o;

using EvilDev.Commons.Testing;

using FluentAssertions;

using Xunit;

namespace ConfigurationManagement.DataAccess.Db4o.Tests
{
    public class Db4OUnitOfWorkSpecification : AutoMockSpecificationFor<Db4OUnitOfWork, IUnitOfWork>
    {
        [Fact]
        public void When_adding_an_entity()
        {
            var entity = Fake<object>();

            Dep<IObjectContainer>(x => x.Setup(oc => oc.Store(entity))
                                        .Verifiable());

            Sut.Add(entity);

            Verify<IObjectContainer>();
        }

        [Fact]
        public void When_updating_an_entity()
        {
            var entity = Fake<object>();

            Dep<IObjectContainer>(x => x.Setup(oc => oc.Store(entity))
                                        .Verifiable());

            Sut.Update(entity);

            Verify<IObjectContainer>();
        }

        [Fact]
        public void When_deleting_an_entity()
        {
            var entity = Fake<object>();

            Dep<IObjectContainer>(x => x.Setup(oc => oc.Delete(entity))
                                        .Verifiable());

            Sut.Remove(entity);

            Verify<IObjectContainer>();
        }

        [Fact]
        public void When_querying_entities()
        {
            var entities = FakeMany<object>().ToList();

            Dep<IObjectContainer>(x => x.Setup(oc => oc.Query<object>())
                                        .Returns(entities)
                                        .Verifiable());

            var results = Sut.Query<object>();

            results.Should().Equal(entities);

            Verify<IObjectContainer>();
        }

         public class WhenDisposed : AutoMockSpecificationFor<Db4OUnitOfWork, IDisposable>
         {
             [Fact]
             public void When_commit_successful()
             {
                 Dep<IObjectContainer>(x =>
                     {
                         x.Setup(oc => oc.Commit()).Verifiable();
                         x.Setup(oc => oc.Dispose()).Verifiable();
                     });

                 Sut.Dispose();

                 Verify<IObjectContainer>();
             }

             [Fact]
             public void Should_rollback_when_commit_unsuccessful()
             {
                 Dep<IObjectContainer>(x =>
                     {
                         x.Setup(oc => oc.Commit()).Throws<Exception>().Verifiable();
                         x.Setup(oc => oc.Rollback()).Verifiable();
                         x.Setup(oc => oc.Dispose()).Verifiable();
                     });

                 Assert.Throws<Exception>(() => Sut.Dispose());

                 Verify<IObjectContainer>();
             }
         }
    }
}