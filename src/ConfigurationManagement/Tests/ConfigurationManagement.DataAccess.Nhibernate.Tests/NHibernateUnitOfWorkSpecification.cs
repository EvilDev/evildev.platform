﻿using System;

using EvilDev.Commons.Testing;

using NHibernate;
using NHibernate.Engine;

using Xunit;

namespace ConfigurationManagement.DataAccess.Nhibernate.Tests
{
    public class NHibernateUnitOfWorkSpecification : AutoMockSpecificationFor<NhibernateUnitOfWork, IUnitOfWork>
    {
        [Fact]
        public void When_unit_of_work_adds_entity()
        {
            var item = Fake<object>();

            Chain<ISessionFactory, ISession>(x => x.OpenSession(), true);

            Dep<ISession>(x => x.Setup(s => s.Save(item)).Verifiable());

            Sut.Add(item);

            Verify<ISession>();
        }

        [Fact]
        public void When_unit_of_work_queries()
        {
            Chain<ISessionFactory, ISession>(x => x.OpenSession(), true);

            Chain<ISession, ITransaction>(x => x.BeginTransaction(), true);

            Chain<ISession, ISessionImplementor>(x => x.GetSessionImplementation(), true);

            Sut.Query<string>();

            Verify<ISessionFactory>();
            Verify<ISession>();
            Verify<ISessionImplementor>();
        }

        [Fact]
        public void When_unit_of_work_removes_entity()
        {
            var item = Fake<object>();

            Chain<ISessionFactory, ISession>(x => x.OpenSession(), true);

            Dep<ISession>(x => x.Setup(s => s.Delete(item)).Verifiable());

            Sut.Remove(item);

            Verify<ISession>();
        }

        [Fact]
        public void When_unit_of_work_updates_entity()
        {
            var item = Fake<object>();

            Chain<ISessionFactory, ISession>(x => x.OpenSession(), true);

            Dep<ISession>(x => x.Setup(s => s.Update(item)).Verifiable());

            Sut.Update(item);

            Verify<ISession>();
        }

        public class WhenNhibernateUnitOfWorkIsDisposed : AutoMockSpecificationFor<NhibernateUnitOfWork, IDisposable>
        {
            [Fact]
            public void Should_commit_transaction()
            {
                // Arrange
                Chain<ISessionFactory, ISession>(x => x.OpenSession());

                Chain<ISession, ITransaction>(x => x.BeginTransaction());

                Dep<ITransaction>(
                    x =>
                        {
                            x.Setup(t => t.Commit()).Verifiable();
                            x.Setup(t => t.Dispose()).Verifiable();
                        });

                Dep<ISession>(x => x.Setup(s => s.Dispose()).Verifiable());

                // Act
                Sut.Dispose();

                // Assert
                Verify<ITransaction>();
                Verify<ISession>();
            }

            [Fact]
            public void Should_rollback_on_error()
            {
                // Arrange
                Chain<ISessionFactory, ISession>(x => x.OpenSession());

                Chain<ISession, ITransaction>(x => x.BeginTransaction());

                Dep<ITransaction>(
                    x =>
                        {
                            x.Setup(t => t.Commit()).Throws<Exception>().Verifiable();
                            x.Setup(t => t.Rollback()).Verifiable();
                            x.Setup(t => t.Dispose()).Verifiable();
                        });

                Dep<ISession>(x => x.Setup(s => s.Dispose()).Verifiable());

                // Act
                Assert.Throws<Exception>(() => Sut.Dispose());

                // Assert
                Verify<ITransaction>();
                Verify<ISession>();
            }
        }
    }
}