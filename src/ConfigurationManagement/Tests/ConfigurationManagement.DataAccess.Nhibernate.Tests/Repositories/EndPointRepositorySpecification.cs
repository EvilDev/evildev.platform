﻿using System.Linq;

using ConfigurationManagement.DataAccess.Entities;
using ConfigurationManagement.DataAccess.Factories;
using ConfigurationManagement.DataAccess.Repositories;
using ConfigurationManagement.Infrastructure;

using EvilDev.Commons.Testing;

using FluentAssertions;

using Moq;

using Xunit;

namespace ConfigurationManagement.DataAccess.Nhibernate.Tests.Repositories
{
    public class EndPointRepositorySpecification : AutoMockSpecificationFor<EndPointRepository, IEndPointRepository>
    {
        public EndPointRepositorySpecification()
        {
            Chain<IUnitOfWorkFactory, IUnitOfWork>(x => x.Create());
        }

        [Fact]
        public void When_adding_endpoint()
        {
            var name = Fake<string>();
            var endpoint = Fake<string>();
            var encryptedEndpoint = Fake<string>();

            EndPointEntity entity = null;

            Dep<IUnitOfWork>(
                x =>
                x.Setup(uow => uow.Add(It.IsAny<EndPointEntity>()))
                 .Callback<EndPointEntity>(e => entity = e)
                 .Verifiable());

            Dep<IEncryptionProvider>(x => x.Setup(ep => ep.Encrypt(endpoint)).Returns(encryptedEndpoint).Verifiable());

            Sut.Add(name, endpoint);

            Verify<IUnitOfWork>();
            Verify<IEncryptionProvider>();

            entity.Should().NotBeNull();
            entity.Name.Should().Be(name);
            entity.Value.Should().Be(encryptedEndpoint);
        }

        [Fact]
        public void When_getting_endpoint_by_name()
        {
            var endpoints = FakeMany<EndPointEntity>().AsQueryable();

            var endpoint = endpoints.First();

            var expectedValue = Fake<string>();

            Dep<IUnitOfWork>(x => x.Setup(uow => uow.Query<EndPointEntity>()).Returns(endpoints).Verifiable());

            Dep<IEncryptionProvider>(
                x => x.Setup(ep => ep.Decrypt(It.IsAny<string>())).Returns(expectedValue).Verifiable());

            var result = Sut[endpoint.Name];

            result.Should().Be(expectedValue);

            Verify<IUnitOfWork>();
            Verify<IEncryptionProvider>();
        }

        [Fact]
        public void When_removing_an_endpoint()
        {
            var endpoints = FakeMany<EndPointEntity>().AsQueryable();
            var endpointToRemove = endpoints.First();
            EndPointEntity removedEndpoint = null;

            Dep<IUnitOfWork>(x => x.Setup(uow => uow.Query<EndPointEntity>()).Returns(endpoints).Verifiable());

            Dep<IUnitOfWork>(
                x =>
                x.Setup(uow => uow.Remove(endpointToRemove))
                 .Callback<EndPointEntity>(e => removedEndpoint = e)
                 .Verifiable());

            Sut.Remove(endpointToRemove.Name);

            removedEndpoint.Should().NotBeNull();
            removedEndpoint.Name.Should().Be(endpointToRemove.Name);

            Verify<IUnitOfWork>();
        }

        [Fact]
        public void When_updating_an_entity()
        {
            var endpoints = FakeMany<EndPointEntity>().AsQueryable();
            var endpointToUpdate = endpoints.First();
            var value = Fake<string>();
            var encryptedValue = Fake<string>();

            EndPointEntity updatedEndpoint = null;

            Dep<IEncryptionProvider>(x => x.Setup(ep => ep.Encrypt(value)).Returns(encryptedValue).Verifiable());

            Dep<IUnitOfWork>(
                x =>
                    {
                        x.Setup(uow => uow.Query<EndPointEntity>()).Returns(endpoints).Verifiable();

                        x.Setup(uow => uow.Update(It.IsAny<EndPointEntity>()))
                         .Callback<EndPointEntity>(u => updatedEndpoint = u)
                         .Verifiable();
                    });

            Sut.Update(endpointToUpdate.Name, value);

            updatedEndpoint.Should().NotBeNull();
            updatedEndpoint.Name.Should().Be(endpointToUpdate.Name);
            updatedEndpoint.Value.Should().Be(encryptedValue);

            Verify<IUnitOfWork>();
            Verify<IEncryptionProvider>();
        }
    }
}