﻿using System.Linq;

using ConfigurationManagement.DataAccess.Entities;
using ConfigurationManagement.DataAccess.Factories;
using ConfigurationManagement.DataAccess.Repositories;

using EvilDev.Commons.Testing;

using FluentAssertions;

using Moq;

using Xunit;

namespace ConfigurationManagement.DataAccess.Nhibernate.Tests.Repositories
{
    public class SettingRepositorySpecification : AutoMockSpecificationFor<SettingRepository, ISettingRepository>
    {
        public SettingRepositorySpecification()
        {
            Chain<IUnitOfWorkFactory, IUnitOfWork>(x => x.Create());
        }

        [Fact]
        public void When_setting_repository_adds_a_setting()
        {
            var setting = Fake<SettingEntity>();

            SettingEntity savedSetting = null;

            Dep<IUnitOfWork>(
                x =>
                x.Setup(uow => uow.Add(It.IsAny<SettingEntity>()))
                 .Callback<SettingEntity>(s => savedSetting = s)
                 .Verifiable());

            Sut.Add(setting.Category, setting.Key, setting.Value);

            savedSetting.Should().NotBeNull();
            savedSetting.Category.Should().Be(setting.Category);
            savedSetting.Key.Should().Be(setting.Key);
            ((object)savedSetting.Value).Should().Be(setting.Value);

            Dep<IUnitOfWork>(x => x.VerifyAll());
        }

        [Fact]
        public void When_setting_repository_gets_settings_by_category()
        {
            var persistentSettings =
                FakeMany<SettingEntity>(entity => entity.With(setting => setting.Category, "TestSettings"))
                    .AsQueryable();

            var expectedResults = persistentSettings.ToDictionary(x => x.Key, x => x.Value);

            Dep<IUnitOfWork>(x => x.Setup(uow => uow.Query<SettingEntity>()).Returns(persistentSettings).Verifiable());

            var actual = Sut["TestSettings"];

            actual.ShouldBeEquivalentTo(expectedResults);

            Dep<IUnitOfWork>(x => x.VerifyAll());
        }

        [Fact]
        public void When_setting_repository_removes_a_setting()
        {
            var settings = FakeMany<SettingEntity>().AsQueryable();

            var settingToRemove = settings.First();

            Dep<IUnitOfWork>(x => x.Setup(uow => uow.Remove(settingToRemove)).Verifiable());

            Dep<IUnitOfWork>(x => x.Setup(uow => uow.Query<SettingEntity>()).Returns(settings).Verifiable());

            Sut.Remove(settingToRemove.Category, settingToRemove.Key);

            Dep<IUnitOfWork>(x => x.VerifyAll());
        }

        [Fact]
        public void When_setting_repository_updates_a_setting()
        {
            var settings = FakeMany<SettingEntity>().AsQueryable();

            var settingToUpdate = settings.First();

            SettingEntity updatedSetting = null;

            Dep<IUnitOfWork>(
                x =>
                x.Setup(uow => uow.Update(It.IsAny<SettingEntity>()))
                 .Callback<SettingEntity>(s => updatedSetting = s)
                 .Verifiable());

            Dep<IUnitOfWork>(x => x.Setup(uow => uow.Query<SettingEntity>()).Returns(settings).Verifiable());

            Sut.Update(settingToUpdate.Category, settingToUpdate.Key, Fake<string>());

            updatedSetting.Should().NotBeNull();
            updatedSetting.Category.Should().Be(settingToUpdate.Category);
            updatedSetting.Key.Should().Be(settingToUpdate.Key);
            ((object)updatedSetting.Value).Should().Be(settingToUpdate.Value);

            Dep<IUnitOfWork>(x => x.VerifyAll());
        }
    }
}