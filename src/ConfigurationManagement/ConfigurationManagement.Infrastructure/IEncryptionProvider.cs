﻿namespace ConfigurationManagement.Infrastructure
{
    public interface IEncryptionProvider
    {
        string Decrypt(string source);

        string Encrypt(string source);
    }
}