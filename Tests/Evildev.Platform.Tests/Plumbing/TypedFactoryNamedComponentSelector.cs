﻿using System.Linq;
using System.Reflection;

using Castle.Facilities.TypedFactory;

using EvilDev.Commons.Testing;

using Xunit;

namespace ConfigurationManagement.Tests.Plumbing
{
    public class TypedFactoryNamedComponentSelectorSpecification : AutoMockSpecificationFor<TypedFactoryNamedComponentSelector, ITypedFactoryComponentSelector>
    {
    }

    public class TypedFactoryNamedComponentSelector : DefaultTypedFactoryComponentSelector
    {
        protected override string GetComponentName(MethodInfo method, object[] arguments)
        {
            if (method.Name == "Create" && method.GetParameters().Any() && method.GetParameters().First().Name == "name" && method.GetParameters().First().ParameterType == typeof(string))
            {
                return (string)arguments[0];
            }
            return base.GetComponentName(method, arguments);
        }
    }
}