﻿using System.Security.Cryptography;
using System.Text;

using ConfigurationManagement.Plumbing;

using FluentAssertions;

using Xunit;

namespace ConfigurationManagement.Tests.Plumbing
{
    public class SymmetricEncryptionProviderSpecification
    {
        [Fact]
        public void Should_be_able_to_encrypt_and_decrypt_back_to_original()
        {
            var algorithm = SymmetricAlgorithm.Create();
            algorithm.GenerateKey();
            algorithm.GenerateIV();

            var key = Encoding.Default.GetString(algorithm.Key);
            var iv = Encoding.Default.GetString(algorithm.IV);

            var sut = new SymmetricEncryptionProvider(key, iv);

            var source = "SomethingReallyCool";

            var encrypted = sut.Encrypt(source);

            encrypted.Should().NotBeEmpty();
            encrypted.Should().NotBe(source);

            var decrypted = sut.Decrypt(encrypted);

            decrypted.Should().NotBeEmpty();
            decrypted.Should().Be(source);
         }
    }
}