﻿using System;
using System.Reflection;

namespace ConfigurationManagement.Tests
{
    public static class ObjectExtensions
    {
        public static T PropertyValue<T>(this object obj, string propertyName)
        {
            return (T)obj.GetType().GetPropertyByName(propertyName).GetValue(obj, null);
        }

        private static PropertyInfo GetPropertyByName(this Type type, string propertyName)
        {
            var prop = type.GetProperty(propertyName) ?? type.BaseType.GetPropertyByName(propertyName);
            return prop;
        }
    }
}