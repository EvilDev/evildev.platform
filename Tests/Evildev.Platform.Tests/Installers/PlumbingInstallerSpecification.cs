﻿using ConfigurationManagement.DataAccess;
using ConfigurationManagement.Facilities;
using ConfigurationManagement.Infrastructure;
using ConfigurationManagement.Installers;

using EvilDev.Commons.Settings;
using EvilDev.Commons.Testing;

using Xunit;

namespace ConfigurationManagement.Tests.Installers
{
    public class PlumbingInstallerSpecification : InstallerSpecificationFor<PlumbingInstaller>
    {
         [Fact]
         public void When_installed()
         {
             Expect<ISettingsProvider>();
             Expect<IUnitOfWork>();
             Expect<IEncryptionProvider>();

             Install();

             ShouldHaveAddedFacility<SettingsFacility>();

             Verify();
         }
    }
}