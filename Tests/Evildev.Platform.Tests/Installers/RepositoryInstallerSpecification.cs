﻿using ConfigurationManagement.DataAccess.Repositories;
using ConfigurationManagement.Installers;

using EvilDev.Commons.Testing;

using Xunit;

namespace ConfigurationManagement.Tests.Installers
{
    public class RepositoryInstallerSpecification : InstallerSpecificationFor<RepositoryInstaller>
    {
        [Fact]
        public void When_installed()
        {
            // Arrange
            Expect<ISettingRepository>();
            Expect<IEndPointRepository>();

            // Act
            Install();

            // Assert
            Verify();
        }
    }
}