﻿using ConfigurationManagement.DataAccess.Factories;
using ConfigurationManagement.Installers;

using EvilDev.Commons.Testing;

using Xunit;

namespace ConfigurationManagement.Tests.Installers
{
    public class FactoryInstallerSpecification : InstallerSpecificationFor<FactoryInstaller>
    {
        [Fact]
        public void When_installed()
        {
            Expect<IUnitOfWorkFactory>();

            Install();

            Verify();
        }
    }
}