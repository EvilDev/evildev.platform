﻿using Castle.Core.Configuration;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;

using ConfigurationManagement.Facilities;
using ConfigurationManagement.Plumbing;

using EvilDev.Commons.Settings;
using EvilDev.Commons.Testing;

using FluentAssertions;

using Moq;

using Xunit;

namespace ConfigurationManagement.Tests.Facilities
{
    public class SettingsFacilitySpecification : AutoMockSpecificationFor<SettingsFacility, IFacility>
    {
         [Fact]
         public void When_facility_is_initialized()
         {
             var registration = Component.For<ISettingsProvider>().Instance(Dep<ISettingsProvider>());

             IRegistration[] registrations = null;

             Dep<IKernel>(x => x.Setup(k => k.Register(It.IsAny<IRegistration[]>())).Callback<IRegistration[]>(reg => registrations = reg));

             Sut.Init(Dep<IKernel>(), Dep<IConfiguration>());

             registrations.Should().NotBeNull()
                 .And.Contain(reg => reg.IsEqualTo(registration));

             SettingsProvider.Current.Should().Be(Dep<ISettingsProvider>());

             Verify<IKernel>();
         }
    }
}