﻿using Castle.MicroKernel.Registration;

namespace ConfigurationManagement.Tests.Facilities
{
    public static class RegistrationExtensions
    {
        public static bool IsEqualTo(this IRegistration reg, IRegistration other)
        {
            var result = reg.GetType() == other.GetType();

            return result;
        }
    }
}